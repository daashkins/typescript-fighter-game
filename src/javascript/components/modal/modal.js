import { createElement } from '../../helpers/domHelper';
export function showModal(m) {
    const root = getModalContainer();
    const modal = createModal(m);
    root.append(modal);
}
function getModalContainer() {
    return document.getElementById('root');
}
function createModal(modal) {
    const layer = createElement({ tagName: 'div', className: 'modal-layer' });
    const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
    const header = createHeader(modal.title, modal.onClose);
    modalContainer.append(header, modal.bodyElement);
    layer.append(modalContainer);
    return layer;
}
function createHeader(title, onClose) {
    const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
    const titleElement = createElement({ tagName: 'span' });
    const closeButton = createElement({ tagName: 'div', className: 'close-btn' });
    titleElement.innerHTML = title.innerHTML;
    closeButton.innerText = '×';
    const close = () => {
        hideModal();
        onClose();
    };
    closeButton.addEventListener('click', close);
    headerElement.append(title, closeButton);
    return headerElement;
}
function hideModal() {
    const modal = document.getElementsByClassName('modal-layer')[0];
    modal.remove();
}
