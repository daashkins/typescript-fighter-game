import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
export function showWinnerModal(fighter) {
    let title = createElement({
        tagName: 'h1',
        className: `modal_head`,
    });
    title.innerHTML = 'GAME OVER';
    let bodyElement = createElement({
        tagName: 'div',
        className: `modal_text`,
    });
    let content = createElement({
        tagName: 'p',
        className: `modal_text_p`,
    });
    content.innerHTML = ' Winner is ' + fighter.name;
    bodyElement.append(content);
    showModal({ title, bodyElement });
}
