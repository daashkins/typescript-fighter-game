import { createElement } from '../helpers/domHelper';
export function createFighterPreview(fighter, position) {
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });
    // todo: show fighter info (image, name, health, etc.)
    const name = createFighterName(fighter.name);
    console.log(name);
    fighterElement.append(name);
    const stat = createStat(fighter);
    fighterElement.append(stat);
    const fighterImage = createFighterImage(fighter);
    fighterElement.append(fighterImage);
    fighterImage.style.height = '400px';
    return fighterElement;
}
export function createFighterImage(fighter) {
    const { source, name } = fighter;
    const attributes = { src: source };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        title: name,
        alt: name,
        attributes,
    });
    return imgElement;
}
function createFighterName(name) {
    const hElement = createElement({
        tagName: 'h2',
        className: 'arena___fighter-name',
    });
    hElement.innerHTML = name;
    return hElement;
}
function createFeature(feature) {
    const pElement = createElement({
        tagName: 'p',
        className: 'fighter-preview___root__stat__block-p',
    });
    pElement.innerHTML = feature;
    return pElement;
}
function createStat(fighter) {
    const fighterStat = createElement({
        tagName: 'div',
        className: `fighter-preview___root__stat`,
    });
    const blockHealth = createBlockHealth(fighter);
    fighterStat.append(blockHealth);
    const blockAttack = createBlockAttack(fighter);
    fighterStat.append(blockAttack);
    const defense = createBlockDefense(fighter);
    fighterStat.append(defense);
    return fighterStat;
}
function createBlock() {
    const fighterBlock = createElement({
        tagName: 'div',
        className: `fighter-preview___root__stat__block`,
    });
    return fighterBlock;
}
function createBlockName(name) {
    const hElement = createElement({
        tagName: 'h3',
        className: 'fighter-preview___root__stat__block-name',
    });
    hElement.innerHTML = name;
    return hElement;
}
function createBlockFeature(feature) {
    const fighterBlockFeature = createElement({
        tagName: 'div',
        className: `fighter-preview___root__stat__block-item`,
    });
    const typeFeature = createFeature(feature);
    fighterBlockFeature.append(typeFeature);
    return fighterBlockFeature;
}
function createBlockHealth(fighter) {
    const block = createBlock();
    const name = createBlockName("Health");
    block.append(name);
    const stat = createBlockFeature(String(fighter.health));
    block.append(stat);
    return block;
}
function createBlockAttack(fighter) {
    const block = createBlock();
    const name = createBlockName("Attack");
    block.append(name);
    const stat = createBlockFeature(fighter.attack);
    block.append(stat);
    return block;
}
function createBlockDefense(fighter) {
    const block = createBlock();
    const name = createBlockName("Defense");
    block.append(name);
    const stat = createBlockFeature(fighter.attack);
    block.append(stat);
    return block;
}
