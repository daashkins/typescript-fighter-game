class Element {
  tagName: string;
  className?: string;
  title?: string;
  alt?: string;
  attributes?: {[key: string]: string};

  constructor(tagName: string, className?: string, title?: string, alt?:string, attributes?: {[key: string]: string}) {
    this.tagName = tagName;
    this.className = className;
    this.title = title;
    this.alt = alt;
    this.attributes = attributes;
  }
}
export default Element;

export function createElement(el: Element) {
  const element = document.createElement(el.tagName);

  if (el.className) {
    const classNames = el.className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }
  
  if (typeof el.attributes !== "undefined") {
    Object.keys(el.attributes!).forEach((key) => element.setAttribute(key, el.attributes![key] as string));
  }
  return element;
}
