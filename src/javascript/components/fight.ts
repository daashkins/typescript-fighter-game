import { controls } from '../../constants/controls';
import Fighter from './fighterPreview';

export async function fight(firstFighter: Fighter, secondFighter: Fighter) : Promise<Fighter> {
  let keysPressed: {[key: string]: boolean} = {};
  let playerOneCriticalHit = true;
  let playerTwoCriticalHit = true;

  let playerOne = firstFighter.health;
  let playerTwo = secondFighter.health;

  document.addEventListener('keydown', (event) => {

    keysPressed[event.code] = true;

    if (getCriticalHit(controls.PlayerOneCriticalHitCombination, keysPressed)){
      if (playerOneCriticalHit === true ) {
        secondFighter.health -= (parseInt(firstFighter.attack)*2);
        // playerTwo -= (parseInt(firstFighter.attack)*2);
        playerOneCriticalHit = false;
        setTimeout(function() { playerOneCriticalHit = true }, 10000);
        adjustHealthBarRight(secondFighter, playerTwo);
      } 
    } if (getCriticalHit(controls.PlayerTwoCriticalHitCombination, keysPressed)){
      if (playerTwoCriticalHit === true ) {
        firstFighter.health -= (parseInt(secondFighter.attack)*2);
        playerTwoCriticalHit = false;
        setTimeout(function() { playerTwoCriticalHit = true }, 10000);
        adjustHealthBarLeft(firstFighter,playerOne);
      } 
    } else if (event.code == controls.PlayerOneAttack) {
        if (keysPressed[controls.PlayerOneBlock]) {
          return;
        }
        if (keysPressed[controls.PlayerTwoBlock]) {
          return;
        } else {
          secondFighter.health -= getDamage(firstFighter, secondFighter);
        }
        adjustHealthBarRight(secondFighter, playerTwo);

    } else if (event.code == controls.PlayerTwoAttack) {
      if (keysPressed[controls.PlayerTwoBlock]) {
        return;
      }
      if (keysPressed[controls.PlayerOneBlock]) {
        return;
        
      } else {
        firstFighter.health -= getDamage(secondFighter,firstFighter);
      }
      adjustHealthBarLeft(firstFighter,playerOne);
    }
  });

  document.addEventListener('keyup', (event) => {
    delete keysPressed[event.code];
  });

  return new Promise((resolve) => {

    // resolve the promise with the winner when fight is over
    let interval = setInterval(function() {
      if (firstFighter.health <= 0 || secondFighter.health <= 0) {
        resolve((firstFighter.health <= 0) ? secondFighter : firstFighter );
        clearInterval(interval);
      } 
      
    }
      ,10);
  });
  
}
function adjustHealthBarLeft(firstFighter: Fighter, playerOne: number) {
 let healthBar = document.getElementById('left-fighter-indicator') as HTMLElement;
 healthBar.style.width = (firstFighter.health <= 0? 0 : (firstFighter.health / playerOne)*100) + '%';
}

function adjustHealthBarRight(secondFighter: Fighter, playerTwo : number) {
  let healthBar = document.getElementById('right-fighter-indicator') as HTMLElement;
  healthBar.style.width = (secondFighter.health <=0 ? 0 : (secondFighter.health/ playerTwo)*100) + '%';
 }

function getCriticalHit(comboArray: string[], keysPressed: object) {
  for (let comboKey of comboArray) {
    if (!keysPressed.hasOwnProperty(comboKey)) {
      return false;
    }
  }
  return true;
}

export function getDamage(attacker: Fighter, defender: Fighter) {
  let hitPower: number = getHitPower(attacker),
      blockPower: number = getBlockPower(defender);
  if (blockPower >= hitPower) {
    return 0;
  } else {
    return hitPower - blockPower;
  }  
}

export function getHitPower(fighter: Fighter) {
  // return hit power
  let criticalHitChance = getRandomInt(1,2);
  const power = parseInt(fighter.attack) * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: Fighter) {
  // return block power
  let dodgeChance = getRandomInt(1,2);
  const power = parseInt(fighter.defense) * dodgeChance ;
  return power;
}

export function getRandomInt(min:number, max:number) {
  return (Math.random() * (max - min) + min);
}
