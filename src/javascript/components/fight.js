import { controls } from '../../constants/controls';
export async function fight(firstFighter, secondFighter) {
    let keysPressed = {};
    let playerOneCriticalHit = true;
    let playerTwoCriticalHit = true;
    let playerOne = firstFighter.health;
    let playerTwo = secondFighter.health;
    document.addEventListener('keydown', (event) => {
        keysPressed[event.code] = true;
        if (getCriticalHit(controls.PlayerOneCriticalHitCombination, keysPressed)) {
            if (playerOneCriticalHit === true) {
                secondFighter.health -= (parseInt(firstFighter.attack) * 2);
                // playerTwo -= (parseInt(firstFighter.attack)*2);
                playerOneCriticalHit = false;
                setTimeout(function () { playerOneCriticalHit = true; }, 10000);
                adjustHealthBarRight(secondFighter, playerTwo);
            }
        }
        if (getCriticalHit(controls.PlayerTwoCriticalHitCombination, keysPressed)) {
            if (playerTwoCriticalHit === true) {
                firstFighter.health -= (parseInt(secondFighter.attack) * 2);
                playerTwoCriticalHit = false;
                setTimeout(function () { playerTwoCriticalHit = true; }, 10000);
                adjustHealthBarLeft(firstFighter, playerOne);
            }
        }
        else if (event.code == controls.PlayerOneAttack) {
            if (keysPressed[controls.PlayerOneBlock]) {
                return;
            }
            if (keysPressed[controls.PlayerTwoBlock]) {
                return;
            }
            else {
                secondFighter.health -= getDamage(firstFighter, secondFighter);
            }
            adjustHealthBarRight(secondFighter, playerTwo);
        }
        else if (event.code == controls.PlayerTwoAttack) {
            if (keysPressed[controls.PlayerTwoBlock]) {
                return;
            }
            if (keysPressed[controls.PlayerOneBlock]) {
                return;
            }
            else {
                firstFighter.health -= getDamage(secondFighter, firstFighter);
            }
            adjustHealthBarLeft(firstFighter, playerOne);
        }
    });
    document.addEventListener('keyup', (event) => {
        delete keysPressed[event.code];
    });
    return new Promise((resolve) => {
        // resolve the promise with the winner when fight is over
        let interval = setInterval(function () {
            if (firstFighter.health <= 0 || secondFighter.health <= 0) {
                resolve((firstFighter.health <= 0) ? secondFighter : firstFighter);
                clearInterval(interval);
            }
        }, 10);
    });
}
function adjustHealthBarLeft(firstFighter, playerOne) {
    let healthBar = document.getElementById('left-fighter-indicator');
    healthBar.style.width = (firstFighter.health <= 0 ? 0 : (firstFighter.health / playerOne) * 100) + '%';
}
function adjustHealthBarRight(secondFighter, playerTwo) {
    let healthBar = document.getElementById('right-fighter-indicator');
    healthBar.style.width = (secondFighter.health <= 0 ? 0 : (secondFighter.health / playerTwo) * 100) + '%';
}
function getCriticalHit(comboArray, keysPressed) {
    for (let comboKey of comboArray) {
        if (!keysPressed.hasOwnProperty(comboKey)) {
            return false;
        }
    }
    return true;
}
export function getDamage(attacker, defender) {
    let hitPower = getHitPower(attacker), blockPower = getBlockPower(defender);
    if (blockPower >= hitPower) {
        return 0;
    }
    else {
        return hitPower - blockPower;
    }
}
export function getHitPower(fighter) {
    // return hit power
    let criticalHitChance = getRandomInt(1, 2);
    const power = parseInt(fighter.attack) * criticalHitChance;
    return power;
}
export function getBlockPower(fighter) {
    // return block power
    let dodgeChance = getRandomInt(1, 2);
    const power = parseInt(fighter.defense) * dodgeChance;
    return power;
}
export function getRandomInt(min, max) {
    return (Math.random() * (max - min) + min);
}
