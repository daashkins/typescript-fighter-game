interface Element {
  tagName: string;
  className?: string;
  title?: string;
  alt?: string;
  attributes?: {[key: string]: string};
}
export default Element;

export function createElement(el: Element) {
  const element = document.createElement(el.tagName);

  if (el.className) {
    const classNames = el.className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }
  
  if (typeof el.attributes !== "undefined") {
    Object.keys(el.attributes!).forEach((key) => element.setAttribute(key, el.attributes![key] as string));
  }
  return element;
}
